# Apache Apex Ann Arbor Meetup

This meetup is intended to work on learning and finding opportunities for the Apex framework.

## Features

The meetup is located in Ann Arbor Michigan.


## Contributing

Anyone and everyone is welcome to contribute content and ideas for the meetup at: ...

## Project information

* Source: http://apache-apex-ann-arbor-meetup.aerobatic.io
* Web: http://www.meetup.com/Apache-Apex-Ann-Arbor-Meetup/


## License

### Major components:

* jQuery: MIT/GPL license
* Modernizr: MIT/BSD license
* Normalize.css: Public Domain

### Everything else:
public domain)